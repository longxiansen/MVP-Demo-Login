package com.test.wzl.mvpdemo_login;

/**
 * Created by Administrator on 2018/7/17.
 */

public interface LoginInteractor {
    void loginError();
    void loginSuccess();
}
