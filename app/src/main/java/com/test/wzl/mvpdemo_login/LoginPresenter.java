package com.test.wzl.mvpdemo_login;

import android.text.TextUtils;
import android.widget.Toast;

import java.util.logging.Handler;

/**
 * Created by Administrator on 2018/7/17.
 */

public class LoginPresenter implements LoginInteractor {
    private ILoginView loginView;

    public LoginPresenter(ILoginView loginView) {
        this.loginView = loginView;
    }

    @Override
    public void loginError() {
        loginView.showLoginError();
    }

    @Override
    public void loginSuccess() {
        loginView.loginSuccess();
    }

    public void login(final String phone, final String password){
        loginView.showLoading();
        new android.os.Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (TextUtils.isEmpty(phone)) {
                    Toast.makeText(MyApp.getContext(), "手机号为空，请重新输入", Toast.LENGTH_SHORT).show();
                }
                if (TextUtils.isEmpty(password)) {
                    Toast.makeText(MyApp.getContext(), "密码为空，请重新输入", Toast.LENGTH_SHORT).show();
                }
                loginSuccess();
            }
        },2000);
    }

    public void onDestory(){
        loginView = null;
    }
}
