package com.test.wzl.mvpdemo_login;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

/**
 * A login screen that offers login via email/password.
 */
public class LoginActivity extends AppCompatActivity implements ILoginView{

    // UI references.
    private AutoCompleteTextView mEmailView;
    private EditText mPasswordView;
    private View mProgressView;
    private View mLoginFormView;
    private EditText edit_email;
    private EditText edit_pwd;
    private ProgressBar pb_login;
    private Button btn_login;
    private LoginPresenter loginPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        loginPresenter = new LoginPresenter(this);
        edit_email = (EditText) findViewById(R.id.email);
        edit_pwd = (EditText) findViewById(R.id.password);
        pb_login = (ProgressBar) findViewById(R.id.login_progress);
        btn_login = (Button) findViewById(R.id.email_sign_in_button);
        btn_login.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                loginPresenter.login(edit_email.getText().toString(),edit_pwd.getText().toString());
            }
        });
    }

    @Override
    public void showLoading() {
        pb_login.setVisibility(View.VISIBLE);
    }

    @Override
    public void showLoginError() {
        pb_login.setVisibility(View.INVISIBLE);
        Toast.makeText(this, "登陆失败", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void loginSuccess() {
        pb_login.setVisibility(View.INVISIBLE);
        startActivity(new Intent(this,MainActivity.class));
    }

    @Override
    protected void onDestroy() {
        loginPresenter.onDestory();
        super.onDestroy();
    }
}

