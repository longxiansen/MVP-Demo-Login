package com.test.wzl.mvpdemo_login;

/**
 * Created by Administrator on 2018/7/17.
 */

public interface ILoginView {
    void showLoading();
    void showLoginError();
    void loginSuccess();
}
