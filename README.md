
/**
 * 什么样的关系呢，就是activity实现IView接口，这个接口可以操纵视图的该变。
 * IPresenter类中利用IView的初始化对象去调用方法以及继承Interactor接口重写逻辑方法
 * 实际上IView和Interactor都是接口，一个用来控制view，一个用来实现逻辑。
 * IPresenter就是类似于一个中转站，在逻辑实现方法中调用操纵视图的方法。
 */